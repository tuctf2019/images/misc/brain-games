# Brain Games -- Misc -- TUCTF2019

[Source](https://gitlab.com/tuctf2019/misc/brain-games)

## Chal Info

Desc: `Time to go back to the basics... you remember those, right?`

Flag: `TUCTF{7H3_M0R3_Y0U_KN0W_G1F}`

## Deployment

[Docker Hub](https://hub.docker.com/r/asciioverflow/brain_games)

Ports: 8888

Example usage:

```bash
docker run -d -p 127.0.0.1:8888:8888 asciioverflow/brain_games:tuctf2019
```

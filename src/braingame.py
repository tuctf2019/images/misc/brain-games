#!/usr/bin/env python
# --------------------------------------------------------------------------
#
#       File: braingame.py          Last Date Updated: November 25 2019
#       Python-based game that emulates Brain Age for CS students
#       Completed:
#           - Ascii art, menu, base convert, bitwise ops, virhistory
#       To Do:
#           -
#
# --------------------------------------------------------------------------
#import packages here
import curses
import random
import sys
from time import sleep

#global screen dimensions, dynamically set per connection.
height = 0
width = 0
lastLine = -1

#start, initialize all the things.
def start(stdscr):
    global lastLine, height, width
    #start color engine, clear the terminal screen.
    curses.start_color()
    stdscr.clear()
    stdscr.refresh()
    #initialize the color identifiers
    #curses.init_pair(num_id, text_color, bg_color)
    curses.init_pair(1, curses.COLOR_CYAN, curses.COLOR_BLACK) #Normal text
    curses.init_pair(2, curses.COLOR_RED, curses.COLOR_BLACK) #Angry text

    #Game time!
    while True:
        #window setup
        height, width = stdscr.getmaxyx()
        half = width/2
        #Generate Dr. Kawashima's window
        head = curses.newwin(height,half,0,0)
        #Generate Text window
        txt = curses.newwin(height,half,0,half+1)
        #set game flags to false
        flags = [False, False, False]

        while not(flags[0] and flags[1] and flags[2]):
            #intro sequence
            renderMenu(head,txt,flags)
            curses.echo() #TODO: what does this do
            choice = txt.getch()
            # loop if choice isnt one of the ones on screen
            while choice not in [49,50,51]:
                #Dr. Kawashima is angry
                renderAngry(head)
                #Ask for a correct menu input
                txt.attron(curses.color_pair(2))
                ny, nx = txt.getyx()
                txty, txtx = txt.getmaxyx()
                txt.addstr(ny+1, txtx/4, "That's not a valid choice...")
                txt.move(ny+2, txtx/4)
                txt.attroff(curses.color_pair(2))
                txt.refresh()
                #Get choice again
                choice = txt.getch()
            # make sure they havent completed the level before
            if choice == 49 and not flags[0]:
                flags[0] = renderConvert(head,txt)
            elif choice == 50 and not flags[1]:
                flags[1] = renderANDOR(head,txt)
            elif choice == 51 and not flags[2]:
                flags[2] = rendervirHistory(head,txt)
        #print the flag!
        renderWin(head,txt)
        break;


def renderMenu(head,txt,flags):
    global lastLine
    renderNormal(head)
    txt.erase()
    txt.border()
    txty,txtx = txt.getmaxyx()
    txt.addstr(txty/4, txtx/4, "Welcome to Brain Games!")
    txt.addstr(txty/4+1, txtx/4, "There are 3 levels to complete.")
    txt.addstr(txty/4+2, txtx/4, "If you complete all three, you'll get the flag! Hoo hoo ha!")
    txt.addstr(txty/4+3, txtx/4, "Select a level from the following:")
    txt.addstr(txty/4+5, txtx/4, "1. Base Conversion x20       [Complete? {}]".format(flags[0]))
    txt.addstr(txty/4+7, txtx/4, "2. Bitwise Operations x20    [Complete? {}]".format(flags[1]))
    txt.addstr(txty/4+9, txtx/4, "3. Virus History             [Complete? {}]".format(flags[2]))
    txt.move(txty/4+10, txtx/4)
    return


def renderConvert(head,txt):
    global lastLine
    renderNormal(head)
    numWrong = 0
    numCorrect = 0
    message = "What is {0} {1} in {2}?"
    txty,txtx = txt.getmaxyx()
    while numWrong < 5 and numCorrect < 20:
        txt.erase()
        txt.border()
        xnum = random.randint(0,255)
        base1 = random.randint(0,3)
        base2 = random.randint(0,3)
        #make sure we're not converting to the same base
        if base1 == base2:
            base2 = (base2 + 1) % 4

        answers = [(bin(xnum)[2::],"(Binary)","Binary"),
                   (oct(xnum),"(Octal)", "Octal"),
                   (str(xnum),"(Decimal)", "Decimal"),
                   (hex(xnum)[2::],"(Hexadecimal)","Hexadecimal")]

        txt.addstr(height/4,txtx/4,message.format(answers[base1][0], answers[base1][1], answers[base2][2]))
        txt.move(height/4+1,txtx/2)
        guess = txt.getstr()
        #allow for other ways to input hex
        if '0x' in guess:
            guess = guess.replace('0x','')
        try:
            if guess == answers[base2][0]:
                renderNormal(head)
                numCorrect += 1
                continue
            else:
                renderAngry(head)
                numWrong += 1
                continue
        except ValueError as e:
             renderAngry(head)
             numWrong += 1
             continue

    if numWrong == 5:
        return False
    else:
        return True


def renderANDOR(head,txt):
    global lastLine
    renderNormal(head)
    numWrong = 0
    numCorrect = 0
    message = "What is {0} {1} {2}?"
    txty,txtx = txt.getmaxyx()
    while numWrong < 5 and numCorrect < 20:
        txt.erase()
        txt.border()
        xnum = random.randint(0,255)
        ynum = random.randint(0,255)
        operation = random.randint(0,2)
        if operation == 0: #AND
            txt.addstr(height/4,txtx/4,message.format(xnum, "AND", ynum))
            answer = xnum & ynum
        elif operation == 1: #OR
            txt.addstr(height/4,txtx/4,message.format(xnum, "OR", ynum))
            answer = xnum | ynum
        else: #XOR
            txt.addstr(height/4,txtx/4,message.format(xnum, "XOR", ynum))
            answer = xnum ^ ynum
        #get user input
        txt.move(height/4+1,txtx/2)
        txt.refresh()
        guess = txt.getstr()
        try:
            if int(guess) == answer:
                renderNormal(head)
                numCorrect += 1
                continue
            else:
                renderAngry(head)
                numWrong += 1
                continue
        except ValueError as e:
            renderAngry(head)
            numWrong += 1
            continue

    if numWrong == 5:
        return False
    else:
        return True


def rendervirHistory(head,txt):
    global lastLine
    renderNormal(head)
    qbank = [("First worm to spread over the Internet, and the first well known use of buffer overflows","Morris Worm"),
             ("The best known of the early macro viruses, mass-mailed itself and caused est. $80 million in damages","Melissa Virus"),
             ("First known virus to erase flash ROM BIOS data, activates on April 26th.","CIH Virus"),
             ("Mass-mailer virus with a VBScript attachment, considered one of the most damaging worms ever","ILOVEYOU Worm"),
             ("Exploited Windows services to spread over a network in 2003, DDoSed Windows Update","Blaster Worm"),
             ("Exploited Windows LSASS, spread over interconnected networks. Removed MyDoom and Bagle in 2004.","Sasser Worm"),
             ("Bridged an airgap to stealth takedown Iranian nuclear development in 2010","Stuxnet"),
             ("A trojan which marked the start of the 'ransomware era' in 2013","CryptoLocker"),
             ("A botnet which spread over the Internet of Things to commit large-scale DDoS attacks from 2016 onward.","Mirai"),
             ("Ransomware which utilized leaked NSA exploits that launched a global attack on Windows computers","WannaCry"),
             ("This malicious command from 1974 eats system resources until a crash occurs","Forkbomb"),
             ("Non-malicious DOS virus that infects files when present in memory. Payload forces all on-screen characters to fall down.","Cascade"),
             ("This virus in 2003 exploited a buffer overflow in Microsoft SQL Server and took less than 10 minutes to reach 90% of vulnerable computers worldwide","Slammer Worm"),
             ("An early botnet which did no initial damage other than block antivirus updates in 2008/2009","Conficker"),
             ("A MS-DOS virus which, when run, infects every DOS executable in the current directory. Plays a tune that lasts approximately 1:49.","Techno"),
             ("Spyware best known for its purple ape mascot. The software is also classified as adware, and was sued twice.","BonziBUDDY"),
             ("One of the only known viruses for the Solaris operating system, developed in 1998.","Solar Sunrise"),
             ("A rogue antivirus program which simulates a malware infection after several weeks running 'unactivated'","Navashield"),
             ("One of the firse known viruses, spread through the ARPANET. Infected systems display the message I'M THE CREEPER: CATCH ME IF YOU CAN.","Creeper"),
             ("The first nematode, created to fight infections of Creeper.","Reaper")]
    correct = [False,False,False,False,False,False,False,False,False,False,False,False,False,False,False,False,False,False,False,False]
    numWrong = 0
    idx = 0
    txty,txtx = txt.getmaxyx()
    while False in correct and numWrong < 5:
        txt.erase()
        txt.border()
        #print question
        txt.addstr(txty/4,txtx/4,qbank[idx][0])
        #print 4 answers
        insertAns = random.randint(0,3)
        mc_sample = random.sample(qbank, k=3)
        mc_sample.insert(insertAns,qbank[idx])
        yval = txty/4+4
        for item in mc_sample:
            txt.addstr(yval, txtx/4, item[1])
            yval += 1
        #get input
        txt.move(yval+1,txtx/4)
        guess = txt.getstr()

        if guess == qbank[idx][1]:
            renderNormal(head)
            correct[idx] = True
            idx += 1
            continue
        else:
            renderAngry(head)
            numWrong += 1
            continue

    if numWrong == 5:
        return False
    else:
        return True


#define Dr.Kawashima normal render
def renderNormal(head):
    global lastLine
    #get our starting coords
    cursorx =  width/4 - 20 #if yalls screens arent big enough you just dont deserve this art tbh.
    cursory = height/2 - 11
    lines = ["          --------\\   /------",
             "        /          \\_/        \\",
             "      /                         \\",
             "    /                             \\",
             "  |       -------------------\\     |",
             " |       /                    \\    |",
             " |      /                      \\    \\",
             "  \\    |                        |   /",
             "  /    |                        |   \\",
             " /    /   /------|     |------\\  \\   \\",
             " |    |  -------/       \\-------  |   |",
             " 0\\---|--/ _---_ \\_____/ _---_ \\--|---/0",
             "0000\\/   \\_______/     \\_______/  \\ /0000",
             "00000            |     |            00000",
             "  000            /     \\            000",
             "    0           /       \\           0",
             "     |          ---------          |",
             "     |                             |",
             "      \\         \\________/        /",
             "        \\                        /",
             "          \\                     /",
             "            \\ _______________ /"]
    for idx in range(0,22):
        #print lines[idx] at position (x, y+idx)
        head.move(cursory, cursorx)
        head.addstr(lines[idx])
        cursory = cursory + 1
    head.border()
    head.refresh()
    return


#define Dr.Kawashima angry render
def renderAngry(head):
    global lastLine
    #get our starting coords
    cursorx =  width/4 - 20 #if yalls screens arent big enough you just dont deserve this art tbh.
    cursory = height/2 - 11
    lines = ["          --------\\   /------",
             "        /          \\_/        \\",
             "      /                         \\",
             "    /                             \\",
             "  |       -------------------\\     |",
             " |       /                    \\    |",
             " |      /                      \\    \\",
             "  \\    |                        |   /",
             "  /    | \\---\\             /---/|   \\",
             " /    /   \\   \\           /   /  \\   \\",
             " |    |    \\___\\         /___/    |   |",
             " 0\\---|--/  (__) \\_____/ (__)  \\--|---/0",
             "0000\\/   \\_______/     \\_______/  \\ /0000",
             "00000            |     |            00000",
             "  000            /     \\            000",
             "    0           /       \\           0",
             "     |          ---------          |",
             "     |                             |",
             "      \\          ________         /",
             "        \\       /        \\       /",
             "          \\                     /",
             "            \\ _______________ /"]
    for idx in range(0,22):
        #print lines[idx] at position (x, y+idx)
        head.move(cursory, cursorx)
        head.addstr(lines[idx])
        cursory = cursory + 1
    head.border()
    head.refresh()
    return


def renderWin(head,txt):
    global lastLine
    renderNormal(head)
    txt.erase()
    txt.border()
    txty,txtx = txt.getmaxyx()
    txt.addstr(txty/4, txtx/4,"Hoo Hoo Ha! Great Job! Here's your flag for all that hard work!")
    txt.addstr(txty/4+3, txtx/4,"flag: TUCTF{7H3_M0R3_Y0U_KN0W_G1F}")
    txt.refresh()
    sleep(20)
    sys.exit()


#init curses wrapper at the start of execution
if __name__ == '__main__':
    try:
        curses.wrapper(start)
    except KeyboardInterrupt as e:
        sys.exit()

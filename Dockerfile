
FROM python:2

# FROM ubuntu:18.04

WORKDIR /usr/src/app

RUN apt -y update && apt -y upgrade
# RUN apt -y install python python-pip socat
RUN apt -y install socat

COPY ./src .

RUN chmod +x /usr/src/app/braingame.py

# ENV TERM xterm

CMD socat TCP-LISTEN:8888,fork,reuseaddr EXEC:"/usr/src/app/braingame.py"
